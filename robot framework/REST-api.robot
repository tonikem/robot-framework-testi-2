*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary


*** Variables ***
${base_url}  http://localhost:18000


*** Test Cases ***
Testaa accounts-rajapintaa
    Create Session    session    ${base_url}
    ${response}    GET On Session    session    accounts/
    Log To Console    \nAccount ID: ${response.json()[0]["_id"]}

    # Haetaan saadun ID-arvon perusteella käyttäjä
    ${response}  GET On Session  session  /accounts/${response.json()[0]["_id"]}
    Log To Console    ${response.json()}
    Should Be Equal As Strings    ${response.json()["email"]}    deidre.hayes@undefined.me

