*** Settings ***
Library  RequestsLibrary
Library  JSONLibrary


*** Variables ***
${base_url}  http://localhost:18000
${res_str}  [{'_id': '0', 'name': {'first': 'Deidre', 'last': 'Hayes'}, 'email': 'deidre.hayes@undefined.me', 'phone': '+1 (839) 577-3100', 'address': '507 Church Avenue, Heil, Wyoming, 1754'}, {'_id': '1', 'name': {'first': 'Maldonado', 'last': 'Sellers'}, 'email': 'maldonado.sellers@undefined.biz', 'phone': '+1 (834) 573-2841', 'address': '286 Hewes Street, Abiquiu, Maine, 2447'}, {'_id': '2', 'name': {'first': 'Elvia', 'last': 'Aguilar'}, 'email': 'elvia.aguilar@undefined.io', 'phone': '+1 (826) 486-2932', 'address': '457 Buffalo Avenue, Caberfae, Connecticut, 2648'}, {'_id': '3', 'name': {'first': 'Chris', 'last': 'Mullins'}, 'email': 'chris.mullins@undefined.org', 'phone': '+1 (884) 425-2397', 'address': '642 Vandalia Avenue, Driftwood, California, 213'}, {'_id': '4', 'name': {'first': 'Vargas', 'last': 'Oneal'}, 'email': 'vargas.oneal@undefined.info', 'phone': '+1 (893) 576-3106', 'address': '413 Bedford Avenue, Bynum, Federated States Of Micronesia, 1793'}]


*** Test Cases ***
Get_accounts
    Create Session  session  ${base_url}
    ${response}  GET On Session  session  /accounts/
    #Log To Console    ${response.json()}
    ${length}  Get Length  ${response.json()}
    Should Be Equal As Numbers    ${length}    5
    Should Be Equal As Strings    ${response.json()[0]["name"]["first"]}  Deidre
    Should Be Equal As Strings    ${response.json()[1]["email"]}   maldonado.sellers@undefined.biz

    FOR  ${index}  IN RANGE  0  5
        Set Suite Variable  ${id}  ${response.json()[${index}]["_id"]}
        Log To Console    ${id}
        Should Be Equal As Numbers  ${id}  ${index}
    END

    Should Be Equal As Strings    ${response.json()}    ${res_str}


Get_one_account
    Create Session  session  ${base_url}
    ${response}  GET On Session    session    /accounts/1  expected_status=200
    Log To Console    ${response.json()["_id"]}
    Should Be Equal As Numbers    ${response.json()["_id"]}  1

Get_cars
    Create Session  session  ${base_url}
    ${response}  GET On Session  session  /accounts/3/cars/
    ${length}  Get Length    ${response.json()}
    Should Be Equal As Numbers    ${length}    3
    Should Be Equal As Strings    ${response.json()[0]["Name"]}    amc rebel sst

